//
// Created by Oleksii on 11/28/19.
//

#pragma once

#include <string>
#include <list>

class Lexer {

public:
	Lexer() = default;
	~Lexer() = default;

	static std::string Trim(const std::string&);
	static std::shared_ptr<std::list<std::string>> ReadFile(const std::string&);
};


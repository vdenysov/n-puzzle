//
// Created by Oleksii on 11/28/19. here where is algor will implement
//

#pragma once

#include <iostream>
#include <deque>
#include "State.h"

class Solver {

public:
	Solver() = default;
	~Solver() = default;
	void run();
	void SetParentState(const State&);
	int32_t countHeuristic(const State&);

private:

	void removeFromOpenList(const State&);
	void setFinalState();
	State findLesserHeuristic();
	bool addStateInOpenList(State);

	State parent;
	State finalState;

	std::deque<State> openList;
	std::deque<int> closedList;
};


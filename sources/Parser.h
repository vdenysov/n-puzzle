//
// Created by Oleksii on 11/28/19.
//

#pragma once

#include <memory>
#include <list>
#include "State.h"

class Parser {
public:
	Parser() = default;
	~Parser() = default;

	void SetDataToParse(std::shared_ptr<std::list<std::string>>);
	[[nodiscard]] const State& GetState() const;
private:
	void RunDataAnalysis() noexcept;
	bool IsUniqueSizeField() noexcept;

	void CheckDataAligment(const std::vector<std::vector<int>>&);
	static std::vector<int32_t> GetNumbers(std::string);
	static std::vector<int32_t> Tokenizer(const std::string& );

	std::shared_ptr<std::list<std::string>> dataToParse {nullptr};
	State based_sate;
	int32_t fieldSize = 0;
};

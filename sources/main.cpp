#include <iostream>
#include "Lexer.h"
#include "Parser.h"
#include "Solver.h"

int main(int ac, const char **av)
{
	if (ac != 2) {
		std::cerr << "Error: wrong usage." << std::endl;
		exit(EXIT_FAILURE);
	}

	Parser parser;
	Solver solver;

	try {
		parser.SetDataToParse(Lexer::ReadFile(av[1]));
	} catch (std::exception &e){
		std::cerr << e.what() << std::endl;
		std::exit(EXIT_FAILURE);
	}
	solver.SetParentState(parser.GetState());

	solver.run();

	return 0;
}

//
// Created by Oleksii on 11/28/19.
//

#include <iostream>
#include "State.h"

State::State(std::int32_t size) {
	field.resize(size);
	for (int i = 0; i < size; ++i) {
		field[i].resize(size);
	}
	field_size = size;
}

int32_t State::getHeuristic() const {
	return heuristic;
}

void State::setHeuristic(int32_t her) {
	heuristic = her;
}

void State::setField(std::vector<std::vector<int32_t>> _field)
{
	field = _field;
	setFieldSize(field.size());
	generateHash();
}

std::vector<std::vector<int32_t>> State::getField() const {
	return field;
}

void State::setFieldSize(int32_t size)
{
	if (size < 2)
		throw std::logic_error("Can`t setup size with value less than 2");
	field_size = size;
}

std::ostream &operator<<(std::ostream &os, const State &obj)
{
	std::string msg[5] = {" ", "|", "Field size: ", "Her: ", " Move Number: "};
	os << msg[2] << obj.field_size << std::endl;

	for (const auto& var: obj.getField()) {
		for (auto var1: var) {
			if (var1 < 10)
				os << msg[0] << var1 << msg[1];
			else
				os << var1 << msg[1];
		}
		os << std::endl;
	}
	os << msg[3] << obj.getHeuristic() << msg[4] << obj.getMoveNumber() <<  std::endl;

	return os;
}

std::pair<int32_t , int32_t > State::getPosition(int32_t number) const {
	for (int32_t x = 0; x < field_size; x++) {
		for (int32_t y = 0; y < field_size; y++) {
			if (field[x][y] == number)
				return {x ,y};
		}
	}
	return {0, 0};
}

int32_t State::getFieldSize() const{
	return field_size;
}

uint64_t State::getHash() const {
	return hash;
}

void State::generateHash()
{
	std::hash<uint32_t >(hash_fn);
	uint32_t val = 7;
	for (size_t i = 0; i < field_size; ++i) {
		for (size_t j = 0; j < field_size; j++) {
			val =  val * 17  + field[i][j];
		}
	}
	std::to_string(val);
	hash = hash_fn(val);
}

bool State::checkDirection(Direction direction) {
	auto zero = getPosition(0);
	switch (direction) {
		case  Direction::UP :
			if (zero.first > 0) return true;
			else break;
		case Direction::DOWN :
			if (zero.first < field_size - 1) return true;
			else break;
		case Direction::LEFT :
			if (zero.second > 0) return true;
			else break;
		case Direction::RIGHT :
			if (zero.second < field_size - 1) return true;
			else break;
	}
	return false;
}

void State::moveZero(Direction direction) {
	auto zero = getPosition(0);

	switch (direction) {
		case  Direction::UP : {
			std::swap(field[zero.first][zero.second], field[zero.first - 1][zero.second]);
			break;
		} case Direction::DOWN : {
			std::swap(field[zero.first][zero.second], field[zero.first + 1][zero.second]);
			break;
		} case Direction::LEFT : {
			std::swap(field[zero.first][zero.second], field[zero.first][zero.second - 1]);
			break;
		} case Direction::RIGHT : {
			std::swap(field[zero.first][zero.second], field[zero.first][zero.second + 1]);
			break;
		}
	}
	setField(field);
}

State *State::getParent() const {
	return parent;
}

void State::setParent(State *s) {
	if (s)
		parent = new State(*s);
}

State::State(const State &ref) {
	if (&ref != this) {
		hash = ref.getHash();
		field_size = ref.getFieldSize();
		field = ref.getField();
		heuristic = ref.getHeuristic();
		moveNumber = ref.getMoveNumber();
		parent = ref.getParent();////
	}
}

int32_t State::getMoveNumber() const {
	return moveNumber;
}

void State::setMoveNumber(int32_t number) {
	moveNumber = number;
}


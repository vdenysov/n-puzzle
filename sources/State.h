//
// Created by Oleksii on 11/28/19.
//

#pragma once

#include <cstdint>
#include <vector>

enum class Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class State {

public:
	State() = default;
	State(int32_t);
	State(const State&);
	~State() = default;

	friend std::ostream& operator<<(std::ostream&, const State&);
	[[nodiscard]] int32_t getHeuristic() const;
	[[nodiscard]] int32_t getFieldSize() const;
	[[nodiscard]] uint64_t getHash() const;
	[[nodiscard]] std::pair<int32_t, int32_t>  getPosition(int32_t) const;
	[[nodiscard]] std::vector<std::vector<int32_t>> getField() const;
	[[nodiscard]] int32_t getMoveNumber() const;
	[[nodiscard]] State *getParent() const;

	void setField(std::vector<std::vector<int32_t>>);
	void setHeuristic(int32_t);
	void setParent(State*);
	void setMoveNumber(int32_t);

	bool checkDirection(Direction);
	void moveZero(Direction);
private:
	State *parent = nullptr;
	void setFieldSize(int32_t);
	void generateHash();
	uint64_t hash = 0;
	int32_t heuristic   = 0;
	int32_t moveNumber  = 0;
	int32_t field_size  = 0;
	std::vector<std::vector<int>> field;
};

//
// Created by Oleksii on 11/28/19.
//
#include <cmath>
#include "Solver.h"

void Solver::run()
{
	setFinalState();
    parent.setHeuristic(countHeuristic(std::ref(parent)));
    parent.setMoveNumber(0);
    parent.setParent(nullptr);
	openList.push_front(parent);

	int i = 0;

	auto child = std::make_unique <State>();

	child->setField(parent.getField());
	child->setParent(NULL);
	child->setHeuristic(parent.getHeuristic());
	child->setMoveNumber(0);
	while (child->getHash() != finalState.getHash()) {
		std::cout << "STEP #" << ++i << std::endl;
		child->setParent(&(*child));

		if (child->checkDirection(Direction::UP)) {
			child->moveZero(Direction::UP);
			addStateInOpenList(*child);
			child->moveZero(Direction::DOWN);
		}
		if (child->checkDirection(Direction::DOWN)) {
			child->moveZero(Direction::DOWN);
			addStateInOpenList(*child);
			child->moveZero(Direction::UP);
		}

		if (child->checkDirection(Direction::LEFT)) {
			child->moveZero(Direction::LEFT);
			addStateInOpenList(*child);
			child->moveZero(Direction::RIGHT);

		}
		if (child->checkDirection(Direction::RIGHT)) {
			child->moveZero(Direction::RIGHT);
			addStateInOpenList(*child);
			child->moveZero(Direction::LEFT);
		}
	    closedList.push_front(child->getHash());
		std::cout << "before delete : " << openList.size() << std::endl;
		removeFromOpenList(*child);
		std::cout << "After : "<< openList.size() << std::endl;

		State lesser = findLesserHeuristic();

		child->setField(lesser.getField());
		child->setMoveNumber(lesser.getMoveNumber());

		if (lesser.getParent() != nullptr)
			child->setParent(&(*lesser.getParent()));
		child->setHeuristic(countHeuristic(*child) + child->getParent()->getMoveNumber());
		child->setMoveNumber(child->getParent()->getMoveNumber() + 1);

		std::cout << "DEBUG: lesser her obj" << *child;
	}

	std::cout << closedList.size() << std::endl;
	std::cout << openList.size() << std::endl;

}

void Solver::SetParentState(const State& state) {
	parent = state;
}

void Solver::setFinalState()
{
	std::vector<std::vector<int32_t >> state;
	int size = parent.getFieldSize();

	state.resize(size);
	for (int i = 0; i < size; ++i)
		state[i].resize(size);

	int number = 1, i ,j , mid = size / 2;

	for (j = 1; j <= mid; j++) ///Number of a curve
	{
#pragma simd
		for (i = j - 1; i < size - j + 1; i++)
			state[j - 1][i] = number++;         ///Set top values vertical
		for (i = j; i < size - j + 1; i++)
			state[i][size - j] = number++;      ///Set right val vertical
		for (i = size - j - 1; i >= j - 1; --i)
			state[size - j][i] = number++;      ///Set down val horizontal
		for (i = size - j - 1; i >= j; i--)
			state[i][j - 1] = number++;         ///Set left val vertical
	}

	if (size > 3) {
		state[mid][(size % 2) ? mid : mid - 1] = 0; ///Set zero in the middle
	}
	finalState.setField(state);
}

int32_t Solver::countHeuristic(const State &candidate)
{
	int32_t heuristic = 0;
	int32_t number = 1;
	for (size_t i = 0; i < candidate.getFieldSize(); ++i ) {
		for (size_t j = 0; j < candidate.getFieldSize(); ++j ) {
			auto cord = candidate.getPosition(number);
			auto cord1 = finalState.getPosition(number);
			heuristic += std::abs(cord.first - cord1.first) + std::abs(cord.second - cord1.second);
			number++;
		}
	}
	return heuristic;
}

bool Solver::addStateInOpenList(State candidate)
{
	auto hash = candidate.getHash();
	for (const auto &node: closedList) {
		if (node == hash) {
			std::cout << "STATE IN CLOSED\n";
			return false;
		}
	}
	for (auto &node: openList) {
		if (node.getHash() == hash) {
			if (candidate.getHeuristic() < node.getHeuristic()) {
				node.setHeuristic(candidate.getHeuristic());
				node.setParent(&(*candidate.getParent()));////Set parent
			}
			return false;
		}
	}
	openList.push_front(candidate);
	return false;
}

//TODO: refactor it -> return statement must be object of State
State Solver::findLesserHeuristic() {
	auto smallest = openList.begin();
	int lesser = openList.begin()->getHeuristic();
	for (auto it = openList.begin(); it != openList.end(); it++)
	{
		if (it->getHeuristic() < lesser && it->getFieldSize() != 0) {
			lesser = it->getHeuristic();
			smallest = it;
			it = openList.begin();
		}
	}
	return *smallest;
}

void Solver::removeFromOpenList(const State &ref) {
	for (auto it = openList.begin(); it != openList.end(); it++) {
		if (it->getHash() == ref.getHash()) {
			openList.erase(it);
			openList.shrink_to_fit();
			break;
		}
	}
}

//
// Created by Oleksii on 11/28/19.
//

#include <fstream>
#include <iostream>
#include "Lexer.h"

std::shared_ptr<std::list<std::string>> Lexer::ReadFile(const std::string& filePath)
{
	std::ifstream file(filePath);
	std::string buffer;
	std::list<std::string> _data;

	if (file.is_open() && !file.eof()) {
		while (std::getline(file, buffer)){
			if (buffer[0] != '#') ////Comment remove this if doesn`t need
				_data.push_back(Trim(buffer));
		}
		_data.remove_if([](const std::string& var) {
			return var.empty();
		});
	} else {
		std::cerr << std::strerror(errno) << std::endl;
		exit(EXIT_FAILURE);
	}
	file.close();
	return std::make_shared<std::list<std::string>>(_data);
}

std::string Lexer::Trim(const std::string& str)
{
	std::string s;

	for (size_t i = 0; i < str.size(); ++i) {
		if (std::isdigit(str[i]) || str[i] == '-' || str[i] == '+') {
			while (std::isdigit(str[i]) || str[i] == '-' || str[i] == '+')
				s += str[i++];
			s += '|';
		} else if (std::isspace(str[i])) {
			while (std::isspace(str[++i]));
			i--;
		} else {
			throw (std::logic_error("LALKA"));
		}
	}
	return s;
}

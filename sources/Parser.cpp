//
// Created by Oleksii on 11/28/19.
//

#include <string>
#include <iostream>
#include <vector>
#include "Parser.h"

void Parser::SetDataToParse(std::shared_ptr<std::list<std::string>> _ptr) {
	if (!_ptr.unique())
		_ptr.reset();
	dataToParse = _ptr;
	if (!IsUniqueSizeField())
		throw std::logic_error("Wrong field size definition");
	RunDataAnalysis();
}

void Parser::RunDataAnalysis() noexcept {

	std::vector<std::vector<int32_t>> field;

	for (const auto& var: *dataToParse) {
		if (var.size() == 2 || !fieldSize)
			fieldSize = std::stoi(var);
		else {
			try {
				field.push_back(Parser::Tokenizer(var));
			}
			catch (std::logic_error &e) {
				std::cerr << e.what() << std::endl;
				std::exit(EXIT_FAILURE);
			}
		}
	}
	try {
		CheckDataAligment(std::ref(field));
		based_sate.setField(field); ////Init first field state
	}  catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		std::exit(EXIT_FAILURE);
	}

}

std::vector<int32_t> Parser::Tokenizer(const std::string& str) {
	std::vector<int32_t> ret;
	for (char i : str) {
		if (!std::isdigit(i) && i != '|' && !std::isspace(i) && i != '-' && i != '+')
			throw std::logic_error("file must contain only numbers or spaces"); //TODO: create custom exceptions or other type error handling
		if (i == '-')
			throw std::logic_error("Negative number not support");
	}
	ret = GetNumbers(str);
	return ret;
}

const State& Parser::GetState() const {
	return based_sate;
}

void Parser::CheckDataAligment(const std::vector<std::vector<int32_t>>& field)
{
	for (const auto& var: field) {
		if (field.size() != fieldSize)
			throw std::logic_error("Error: wrong field size (x)");
	}
	if (field.size() != fieldSize) {
		throw std::logic_error("Error: wrong field size (y)");
	}
}

bool Parser::IsUniqueSizeField() noexcept {
	int32_t ret = 0;

	for (const auto& var: *dataToParse) {
		if (var.size() == 2) //// size == 2 because [1] -> size [|] -> delimeter
			ret++;
	}
	return ret == 1;
}
////Getting number from updated string
std::vector<int32_t> Parser::GetNumbers(std::string ref) {
	std::vector<int32_t> field_x;

	while (!ref.empty()) {
		std::string number = ref.substr(0, ref.find_first_of( '|'));
		ref.erase(0, ref.find_first_of( '|') + 1);
		ref.shrink_to_fit();
		field_x.push_back(std::stoi(number));
		number.clear();
	}
	return field_x;
}
